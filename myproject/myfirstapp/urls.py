from django.urls import path
from . import views
urlpatterns = [
    path('', views.index),
    path('hello',views.say_hello),
    path ('bye/<name>',views.say_bye_to),
    path('number/<int:number>',views.say_number)
]
