from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.

def index(request):
    return HttpResponse("<h1>Welcome to my App!</h1>")

def say_hello(request):
    return HttpResponse("Hello!") 
def say_bye_to(request, name):
    return HttpResponse("Bye, " + name)

def say_number(request,number = 23):
    return HttpResponse("You are number " + str(number))
